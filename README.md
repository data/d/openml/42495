# OpenML dataset: delays_zurich_transport

https://www.openml.org/d/42495

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Zurich public transport delay data 2016-10-30 03:30:00 CET - 2016-11-27 01:20:00 CET cleaned and prepared at Open Data Day 2017. For this version, the task was downsampled to 0.5 percent. Some features were recoded as factors and some new time features were computed.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42495) of an [OpenML dataset](https://www.openml.org/d/42495). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42495/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42495/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42495/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

